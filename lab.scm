;add 
(define add (lambda (m n s z)(m s(n s z))))
;subtraction
(define subtraction (lambda (m n) n pred m))
;AND
(define AND (lambda (m n) (lambda (a b) (n (m a b) b))))
;OR 
(define OR (lambda (m n) (lambda (a b) (n a(m a b) ))))
;NOT	
(define NOT(lambda (m) (lambda (a b) (m b a ))))
;isZero
(define IsZero(lambda (n) (lambda (x false) true)))
;GEQ
(define GEQ(lambda (m n)(NOT(AND( (NOT(EQ m n)) (LEQ m n) )))))
;LEQ
(define LEQ(lambda (m n) (IsZero ( subtraction m n) )))

	 
	 